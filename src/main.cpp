#include <Arduino.h>
#include <Streaming.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <Arduino.h>
#include <Adafruit_BME280.h>

//#define DEBUG
#ifdef DEBUG
  #define DBG_PRINTLN(message)\
  {                           \
    Serial.println((message));\
    Serial.flush();           \
  }

  #define DBG_PRINT(message)  \
  {                           \
    Serial.print((message));  \
    Serial.flush();           \
  }
#else
  #define DBG_PRINTLN(message)
  #define DBG_PRINT(message)
#endif

const byte BME_SDA_PIN = D2;
const byte BME_SCL_PIN = D1;

const byte RAINDROP_PWR_PIN = D6;
const byte RAINDROP_D_PIN = D7;

const byte LIGHTSENSOR_PWR_PIN = D5;
const byte LIGHTSENSOR_PIN = A0;

const char *WIFI_SSID       = "SkyNET";
const char *WIFI_PASSWD     = "**********";
const IPAddress IP_ADDRESS(192, 168, 1, 200);
const IPAddress GATEWAY_ADDRESS(192, 168, 1, 1);
const IPAddress SUBNET_MASK(255, 255, 255, 0);
const IPAddress DNS_ADDRESS(GATEWAY_ADDRESS);
const uint16_t WIFI_CONNECT_TIMEOUT = 10 * 1000;

const char *MQTT_SERVER         = "192.168.1.254";
const char *MQTT_ID             = "OWR_Front_Door";
const char *MQTT_USER           = "weather_reporter";
const char *MQTT_PASSWD         = "*************";
const char *MQTT_REPORT_TOPIC   = "nyirad/sensors/weather";

const unsigned long DEEP_SLEEP_BETWEEN_REPORTS = 5 * 60 * 1000000; // microsecs (5 mins)

const char *NTP_TIMESERVER     = "europe.pool.ntp.org";
const uint16_t  NTP_OFFSET         = 3600; // CEST timezone offset to UTC
const uint64_t  NTP_MIN_EPOCH_TIME = 1538000000;

WiFiUDP ntpUdp;
NTPClient timeClient(ntpUdp, NTP_TIMESERVER, NTP_OFFSET, 0);

WiFiClient espClient;
PubSubClient mqtt(MQTT_SERVER, 1883, NULL, espClient);

Adafruit_BME280 bme;
float temperature = 0;
float pressure = 0;
float humidity = 0;

int light_intensity = 0;

bool is_raining = false;
uint64_t timestamp;

enum state
{
    INIT = 0, CONNECT_WIFI, CONNECT_MQTT, MEASURE, TRANSMIT, DISCONNECT_ALL, DEEP_SLEEP
};

typedef state (*fnc)();
fnc fnc_array[DEEP_SLEEP + 1];
state currentState;

state init_fnc()
{
    ESP.wdtEnable(WDTO_8S);

    #ifdef DEBUG
        Serial.begin(115200);
    #endif
    timeClient.begin();

    Wire.begin(BME_SDA_PIN, BME_SCL_PIN);
    if(!bme.begin())
    {
        DBG_PRINTLN(F("Error. BME280 not found."));
        return DEEP_SLEEP;
    }
    bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X1, // temperature
                    Adafruit_BME280::SAMPLING_X1, // pressure
                    Adafruit_BME280::SAMPLING_X1, // humidity
                    Adafruit_BME280::FILTER_OFF   );

    pinMode(RAINDROP_D_PIN, INPUT);
    pinMode(RAINDROP_PWR_PIN, OUTPUT);
    pinMode(LIGHTSENSOR_PWR_PIN, OUTPUT);

    return CONNECT_WIFI;
}

state connect_wifi_fnc()
{
    WiFi.mode(WIFI_STA);
    WiFi.config(IP_ADDRESS, GATEWAY_ADDRESS, SUBNET_MASK, DNS_ADDRESS);
    DBG_PRINT(F("Connecting to the WiFi network... "));
    WiFi.begin(WIFI_SSID, WIFI_PASSWD);

    long connectStartTime = millis();
    while(WiFi.status() != WL_CONNECTED)
    {
        if(millis() - connectStartTime > WIFI_CONNECT_TIMEOUT)
        {
            DBG_PRINTLN(F("Failed (Timeout)"));
            return DEEP_SLEEP;
        }

        ESP.wdtFeed();
        delay(50);
    }

    DBG_PRINTLN(F("Connected."));
    return CONNECT_MQTT;
}

state connect_mqtt_fnc()
{
    DBG_PRINT(F("Connecting to the MQTT server... "));

    byte retryCount = 0;
    do
    {
        if(WiFi.status() != WL_CONNECTED)
        {
          DBG_PRINTLN(F("WiFi connection lost. Trying to reconnect..."));
          return CONNECT_WIFI;
        }

        mqtt.connect(MQTT_ID, MQTT_USER, MQTT_PASSWD);
        #ifdef DEBUG
          Serial << F("Result_Code: ") << mqtt.state() << F(" (Retries: ") << retryCount << ')' << endl;
          Serial.flush();
        #endif

        if(!mqtt.connected() && retryCount >= 5)
        {
            DBG_PRINTLN(F("Giving up..."))
            return DEEP_SLEEP;
        }
        ++retryCount;

        ESP.wdtFeed();
        delay(50);
    }
    while(!mqtt.connected());

    DBG_PRINTLN(F("Connected."));
    return MEASURE;
}

state measure_fnc()
{
    digitalWrite(RAINDROP_PWR_PIN, HIGH);
    delay(500);
    is_raining = !(bool)digitalRead(RAINDROP_D_PIN);
    DBG_PRINTLN(is_raining);
    digitalWrite(RAINDROP_PWR_PIN, LOW);

    digitalWrite(LIGHTSENSOR_PWR_PIN, HIGH);
    delay(200);
    light_intensity = analogRead(LIGHTSENSOR_PIN);
    digitalWrite(LIGHTSENSOR_PWR_PIN, LOW);

    bme.takeForcedMeasurement();
    temperature = bme.readTemperature();
    pressure = bme.readPressure() / 100; // Convert pressure to hPa (from Pa)
    humidity = bme.readHumidity();

    // The loop below is a workaround. Sometimes the timestamp is equal
    // to NTP_OFFSET or NTP_OFFSET + 1. I don't know why..
    while(timestamp < NTP_MIN_EPOCH_TIME)
    {
      timeClient.forceUpdate();
      timestamp = timeClient.getEpochTime();

      if(timestamp < NTP_MIN_EPOCH_TIME)
        delay(100);
    }

    return TRANSMIT;
}

state transmit_fnc()
{
    DBG_PRINTLN(F("Transmit JSON..."));
    String json_string = (
        String(F("{\"timestamp\":")) + String((unsigned long)timestamp) + String(F(","))
        + String(F("\"temperature\":")) + String(temperature, 2) + String(F(","))
        + String(F("\"humidity\":")) + String(humidity, 1) + String(F(","))
        + String(F("\"air_pressure\":")) + String(pressure, 3) + String(F(","))
        + String(F("\"light_intensity\":")) + String(light_intensity) + String(F(","))
        + String(F("\"is_raining\":")) + String(is_raining) + String("}")
    );

    if(!mqtt.connected())
    {
      DBG_PRINTLN(F("MQTT connection lost. Trying to reconnect..."));
      return CONNECT_MQTT;
    }

    mqtt.loop();
    mqtt.publish(MQTT_REPORT_TOPIC, json_string.c_str(), true);
    mqtt.loop();

    DBG_PRINT(F("JSON sent: "));
    DBG_PRINTLN(json_string);

    return DISCONNECT_ALL;
}

state disconnect_all_fnc()
{
    mqtt.disconnect();
    WiFi.disconnect(true);

    return DEEP_SLEEP;
}

state deep_sleep_fnc()
{
    DBG_PRINTLN(F("Entering deep sleep..."));
    bme.setSampling(Adafruit_BME280::MODE_SLEEP,
                    Adafruit_BME280::SAMPLING_NONE, // temperature
                    Adafruit_BME280::SAMPLING_NONE, // pressure
                    Adafruit_BME280::SAMPLING_NONE, // humidity
                    Adafruit_BME280::FILTER_OFF   );

    WiFi.forceSleepBegin(DEEP_SLEEP_BETWEEN_REPORTS);
    ESP.deepSleep(DEEP_SLEEP_BETWEEN_REPORTS);
    return INIT;
}


void setup() {
    fnc_array[INIT]             = init_fnc;
    fnc_array[CONNECT_WIFI]     = connect_wifi_fnc;
    fnc_array[CONNECT_MQTT]     = connect_mqtt_fnc;
    fnc_array[MEASURE]          = measure_fnc;
    fnc_array[TRANSMIT]         = transmit_fnc;
    fnc_array[DISCONNECT_ALL]   = disconnect_all_fnc;
    fnc_array[DEEP_SLEEP]       = deep_sleep_fnc;
    currentState = INIT;
}

void loop()
{
    DBG_PRINT(F("Current State: "));
    DBG_PRINTLN(currentState);

    currentState = fnc_array[currentState]();
    ESP.wdtFeed();
}
